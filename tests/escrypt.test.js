const fs = require('fs');
const ESCrypt = require('../escrypt');

function prepare() {
  if (!fs.existsSync('./testData') || !fs.lstatSync('./testData').isDirectory()) {
    fs.mkdirSync('./testData');
  }
}

function cleanup() {
  fs.unlinkSync('./testData/test-private.pem');
  fs.unlinkSync('./testData/test-public.pem');
  fs.unlinkSync('./testData/test.config');
}

describe('Tests for the ESCrypt module', () => {
  let esCrypt = false;
  beforeAll(prepare);
  afterAll(cleanup);
  describe('Generate a new instance of the ESCrypt module', () => {
    test('It should return a new instance of ESCrypt', () => {
      expect(esCrypt = new ESCrypt({
        environment: 'test',
        keyDirectory: './testData/',
        configurationDirectory: './testData/',
        configurationType: 'json'
      })).toBeTruthy();
    });
    test('It should fail due to missing parameters', () => {
      expect(() => {
        const ding = new ESCrypt({
          environment: 'test'
        });
      }).toThrowError();
    });
    test('It should fail due to additional paremeters', () => {
      expect(() => {
        const ding = new ESCrypt({
          environment: 'test',
          keyDirectory: './testData/',
          configurationDirectory: './testData/',
          configurationType: 'json',
          additionalThrow: 'yes please'
        });
      }).toThrowError();
    });
    test('It should fail due to a non existent key directory', () => {
      expect(() => {
        const ding = new ESCrypt({
          environment: 'test',
          keyDirectory: './badDataDirectory/',
          configurationDirectory: './testData/',
          configurationType: 'json'
        });
      }).toThrow();
    });
    test('It should fail due to a non existent config directory', () => {
      expect(() => {
        const ding = new ESCrypt({
          environment: 'test',
          keyDirectory: './testData/',
          configurationDirectory: './badDataDirectory/',
          configurationType: 'json'
        });
      }).toThrow();
    });
    test('It should return a new instance without generating keys', () => {
      expect(new ESCrypt({
        environment: 'test',
        keyDirectory: './testData/',
        configurationDirectory: './testData/',
        configurationType: 'json'
      })).toBeTruthy();
    });
  });
  describe('Checks available methods and files', () => {
    test('Encrypt method should exist', () => {
      expect(typeof esCrypt.encrypt).toEqual('function');
    });
    test('Decrypt method should exist', () => {
      expect(typeof esCrypt.decrypt).toEqual('function');
    });
    test('Private key should exist', () => {
      expect(fs.existsSync('./testData/test-private.pem')).toEqual(true);
    });
    test('Public key should exist', () => {
      expect(fs.existsSync('./testData/test-public.pem')).toEqual(true);
    });
    test('Configuration should exist', () => {
      expect(fs.existsSync('./testData/test.config')).toEqual(true);
    });
  });
  describe('Validate Encryption', () => {
    test('It should encrypt my secret', () => {
      expect(esCrypt.encrypt('testKey', 'someRandomSecret')).toBeUndefined();
    });
  });

  describe('Validate configuration containes encrypted secret', () => {
    test('It should contain an encrypted secret', () => {
      expect(Object.keys(JSON.parse(fs.readFileSync('./testData/test.config'))).indexOf('testKey')).toEqual(0);
      expect(JSON.parse(fs.readFileSync('./testData/test.config')).testKey).not.toEqual('someRandomSecret');
      expect(JSON.parse(fs.readFileSync('./testData/test.config')).testKey).not.toEqual('');
    });
  });

  describe('Validation Decryption', () => {
    test('It should decrypt my secret', () => {
      expect(esCrypt.decrypt('testKey')).toEqual('someRandomSecret');
    });
  });
});
