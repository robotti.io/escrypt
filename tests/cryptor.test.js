const Cryptor = require('../lib/cryptor');
const fs = require('fs');

const keys = {
  privateKey: './testData/privateKey.pem',
  publicKey: './testData/publicKey.pem'
};

function prepare() {
  if (!fs.existsSync('./testData') || !fs.lstatSync('./testData').isDirectory()) {
    fs.mkdirSync('./testData');
  }
}

function cleanup() {
  fs.unlinkSync(keys.privateKey);
  fs.unlinkSync(keys.publicKey);
}

describe('Cryptor Tests without a passphrase', () => {
  const testData = {};
  beforeAll(prepare);
  afterAll(cleanup);
  describe('Generate a new instance of the Cryptor class', () => {
    test('It should return a new instance of the class', () => {
      expect(testData.cryptor = new Cryptor(keys)).toBeTruthy();
    });
  });
  describe('Generate a new key pair', () => {
    test('It should return a new key pair', () => {
      expect(testData.cryptor.generateKeys()).toEqual({ privateKeyFile: './testData/privateKey.pem', publicKeyFile: './testData/publicKey.pem' });
    });
  });
  describe('Encrypt a secret', () => {
    test('It should return an encrypted secret', () => {
      expect(testData.encryptedSecret = testData.cryptor.encrypt('Random Secret')).toBeTruthy();
      expect(typeof testData.encryptedSecret).toEqual('string');
    });
  });
  describe('Decrypt an encrypted secret', () => {
    test('It should return the decrypted secret', () => {
      expect(testData.cryptor.decrypt(testData.encryptedSecret)).toEqual('Random Secret');
    });
  });
});
