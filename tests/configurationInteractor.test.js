const fs = require('fs');
const ConfigurationInteractor = require('../lib/configurationInteractor');

function prepare() {
  if (!fs.existsSync('./testData') || !fs.lstatSync('./testData').isDirectory()) {
    fs.mkdirSync('./testData');
  }
  fs.writeFileSync('./testData/test.json', '');
}

function cleanup() {
  fs.unlinkSync('./testData/test.json');
}

describe('Tests for the ConfigurationInteractor class', () => {
  const testData = {};
  beforeAll(prepare);
  afterAll(cleanup);
  describe('Generate a new instance of the ConfigurationInteractor class', () => {
    test('It should return a new instance of the class', () => {
      expect(testData.configurationInteractor = new ConfigurationInteractor({
        directory: './testData/',
        filename: 'test.json',
        type: 'json'
      })).toBeTruthy();
    });
  });
  describe('Store a secret', () => {
    test('It should store a secret', () => {
      expect(testData.configurationInteractor.storeEncryptedSecret('test', '32')).toBeUndefined();
    });
  });

  describe('Retrieve a secret', () => {
    test('It should retrieve a secret', () => {
      expect(testData.configurationInteractor.retrieveEncryptedSecret('test')).toEqual('32');
    });
  });
});
