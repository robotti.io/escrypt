module.exports = {
  'collectCoverageFrom': ['escrypt.js', 'lib/**/*.js', '!**/node_modules/**'],
  'coverageReporters': ['html', 'text', 'text-summary', 'cobertura'],
  'testMatch': ['**/*.test.js']
}