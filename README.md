# escrypt

[ESCrypt](https://gitlab.com/robotti.io/escrypt): An enterprise secret encryption/decryption package designed to enable build time secret encryption capabilities

[![pipeline status](https://gitlab.com/robotti.io/escrypt/badges/production/pipeline.svg)](https://gitlab.com/robotti.io/escrypt/-/commits/production)

[![coverage report](https://gitlab.com/robotti.io/escrypt/badges/production/coverage.svg)](https://gitlab.com/robotti.io/escrypt/-/commits/production)

[![Latest Release](https://gitlab.com/robotti.io/escrypt/-/badges/release.svg)](https://gitlab.com/robotti.io/escrypt/-/releases)

## Install via NPM
```
npm intall @robotti.io/escrypt
```

## Usage

### Create Encryption Key Pair

This will create a key pair with the name `environment`-public.pem and `environment`-private.pem. It will also create an empty json file called `environment`.config to to store encrypted secrets in.
```javascript
const ESCrypt = require('escrypt');
const mySecretEncryptor = new ESCrypt({
  environment: 'local',
  keyDirectory: './',
  configurationDirectory: './',
  configurationType: 'json'
});
```

### Encrypt a secret

To encrypt a secret only the public key is required. The cryptor will encrypt the secret with the the public key and store it in the `environment`.config file.
```javascript
const ESCrypt = require('escrypt');
const mySecretEncryptor = new ESCrypt({
  environment: 'local',
  keyDirectory: './',
  configurationDirectory: './',
  configurationType: 'json'
});
mySecretEncryptor.encrypt('secretKey', '5up3rS3cr37$%!');
```

### Decrypt a secret

To decrypt a secret the private key is required. The cryptor will decrypt the value of the key in the `environment`.config file and return the decrypted value.
```javascript
const ESCrypt = require('escrypt');
const mySecretEncryptor = new ESCrypt({
  environment: 'local',
  keyDirectory: './',
  configurationDirectory: './',
  configurationType: 'json'
});
const decryptedSecret = mySecretEncryptor.decrypt('secretKey');
```
